function [ TI_pair, alpha_pair, TI_elec, alpha_elec ] = TIPairEmotion(S1,S2)

    % Compute TI of 2 subjects from 1-20 Hz with 0.25 resolution
    % Compute Alpha coherence of 2 subjects from 8-12 Hz with 0.25 resolution
    
    % Outout
    %  - TI_pair = vector of numeric value TI for each pair of electrode
    %  - alpha_pair = vector of numberic value alpha coherence for each pair of electrode
    
    if isempty(S1)
        TI_pair = 0;
        alpha_pair = 0;
        TI_elec = zeros(1,14);
        alpha_elec = zeros(1,14);
        return
    end
    threshold_epoch = 20;
    if size(S1,1) < 128*(threshold_epoch-1)
        TI_pair = 0;
        alpha_pair = 0;
        TI_elec = zeros(1,14);
        alpha_elec = zeros(1,14);
        return
    end
    
    TI_pair = [];
    alpha_pair = [];
    
    for elec = 1:14
        [TI, alpha] = TIComputationEmotion(S1(:,elec),S2(:,elec));
        TI_pair = [TI_pair TI];
        alpha_pair = [alpha_pair alpha];
    end
    
    TI_elec = TI_pair;
    alpha_elec = alpha_pair;
    TI_pair = sum(TI_pair);
    alpha_pair = sum(alpha_pair);
end

