function [ x,y ] = CommonEpochCascadeEmotion( folder1, folder2, emotion_chosen )
%   This function receive 2 folder path and return 2 signal x and y 
%   x and y are concatenated .eph file of the common epoch in each folder

    if emotion_chosen < 5
        %%%% compare accepted epoch %%%%

        flag = readtable([folder1 filesep 'flag.csv']);
        flag = table2cell(flag);
        epoch_array = cell(length(flag),4);
        epoch_array(:,1) = flag;

        epoch_list = dir(fullfile(folder1, '*.eph'));
        epoch_list = struct2cell(epoch_list);
        epoch_list = strcat(epoch_list(2,:),filesep,epoch_list(1,:));

        epoch_to_add = 1;
        for i = 1:length(flag)
            if epoch_array{i,1} == 1 
                epoch_array{i,2} = epoch_list{epoch_to_add};
            end
            epoch_to_add = epoch_to_add + 1;
        end

        flag = readtable([folder2 filesep 'flag.csv']);
        flag = table2cell(flag);
        epoch_array(:,3) = flag;

        epoch_list = dir(fullfile(folder2, '*.eph'));
        epoch_list = struct2cell(epoch_list);
        epoch_list = strcat(epoch_list(2,:),filesep,epoch_list(1,:));

        epoch_to_add = 1;
        for i = 1:length(flag)
            if epoch_array{i,3} == 1 
                epoch_array{i,4} = epoch_list{epoch_to_add};
            end
            epoch_to_add = epoch_to_add + 1;
        end


        epoch_accepted = [];
        for i = 1:length(flag)
            if epoch_array{i,1} && epoch_array{i,3} 
                epoch_accepted = [epoch_accepted i];
            end
        end

        %%%% compare emotion %%%%
        emotion1 = readtable([folder1 filesep 'emotion.csv']);
        emotion1 = table2cell(emotion1);
        emotion2 = readtable([folder2 filesep 'emotion.csv']);
        emotion2 = table2cell(emotion2);

        epoch_to_use = [];
        for e = 1:length(epoch_accepted)
            position = epoch_accepted(e);
            if emotion1{position} == emotion_chosen && emotion2{position} == emotion_chosen
                epoch_to_use = [epoch_to_use position];
            end
        end

        %%%% cascade verified epoch %%%%
        x = [];
        for i = 1:length(epoch_to_use)
            [~,~,~,signal] = openeph(epoch_array{epoch_to_use(i),2});
            x = [x ; signal];
        end

        y = [];
        for i = 1:length(epoch_to_use)
            [~,~,~,signal] = openeph(epoch_array{epoch_to_use(i),4});
            y = [y ; signal];
        end
        
    elseif emotion_chosen == 5
        %%%% compare accepted epoch %%%%

        flag = readtable([folder1 filesep 'flag.csv']);
        flag = table2cell(flag);
        epoch_array = cell(length(flag),4);
        epoch_array(:,1) = flag;

        epoch_list = dir(fullfile(folder1, '*.eph'));
        epoch_list = struct2cell(epoch_list);
        epoch_list = strcat(epoch_list(2,:),filesep,epoch_list(1,:));

        epoch_to_add = 1;
        for i = 1:length(flag)
            if epoch_array{i,1} == 1 
                epoch_array{i,2} = epoch_list{epoch_to_add};
            end
            epoch_to_add = epoch_to_add + 1;
        end

        flag = readtable([folder2 filesep 'flag.csv']);
        flag = table2cell(flag);
        epoch_array(:,3) = flag;

        epoch_list = dir(fullfile(folder2, '*.eph'));
        epoch_list = struct2cell(epoch_list);
        epoch_list = strcat(epoch_list(2,:),filesep,epoch_list(1,:));

        epoch_to_add = 1;
        for i = 1:length(flag)
            if epoch_array{i,3} == 1 
                epoch_array{i,4} = epoch_list{epoch_to_add};
            end
            epoch_to_add = epoch_to_add + 1;
        end


        epoch_accepted = [];
        for i = 1:length(flag)
            if epoch_array{i,1} && epoch_array{i,3} 
                epoch_accepted = [epoch_accepted i];
            end
        end

        %%%% compare emotion %%%%
        emotion1 = readtable([folder1 filesep 'emotion.csv']);
        emotion1 = table2cell(emotion1);
        emotion2 = readtable([folder2 filesep 'emotion.csv']);
        emotion2 = table2cell(emotion2);

        epoch_to_use = [];
        for e = 1:length(epoch_accepted)
            position = epoch_accepted(e);
            if emotion1{position} < 3 && emotion2{position} < 3 && emotion1{position} > 0 && emotion2{position} > 0
                epoch_to_use = [epoch_to_use position];
            end
        end

        %%%% cascade verified epoch %%%%
        x = [];
        for i = 1:length(epoch_to_use)
            [~,~,~,signal] = openeph(epoch_array{epoch_to_use(i),2});
            x = [x ; signal];
        end

        y = [];
        for i = 1:length(epoch_to_use)
            [~,~,~,signal] = openeph(epoch_array{epoch_to_use(i),4});
            y = [y ; signal];
        end
        
    elseif emotion_chosen == 6
        %%%% compare accepted epoch %%%%

        flag = readtable([folder1 filesep 'flag.csv']);
        flag = table2cell(flag);
        epoch_array = cell(length(flag),4);
        epoch_array(:,1) = flag;

        epoch_list = dir(fullfile(folder1, '*.eph'));
        epoch_list = struct2cell(epoch_list);
        epoch_list = strcat(epoch_list(2,:),filesep,epoch_list(1,:));

        epoch_to_add = 1;
        for i = 1:length(flag)
            if epoch_array{i,1} == 1 
                epoch_array{i,2} = epoch_list{epoch_to_add};
            end
            epoch_to_add = epoch_to_add + 1;
        end

        flag = readtable([folder2 filesep 'flag.csv']);
        flag = table2cell(flag);
        epoch_array(:,3) = flag;

        epoch_list = dir(fullfile(folder2, '*.eph'));
        epoch_list = struct2cell(epoch_list);
        epoch_list = strcat(epoch_list(2,:),filesep,epoch_list(1,:));

        epoch_to_add = 1;
        for i = 1:length(flag)
            if epoch_array{i,3} == 1 
                epoch_array{i,4} = epoch_list{epoch_to_add};
            end
            epoch_to_add = epoch_to_add + 1;
        end


        epoch_accepted = [];
        for i = 1:length(flag)
            if epoch_array{i,1} && epoch_array{i,3} 
                epoch_accepted = [epoch_accepted i];
            end
        end

        %%%% compare emotion %%%%
        emotion1 = readtable([folder1 filesep 'emotion.csv']);
        emotion1 = table2cell(emotion1);
        emotion2 = readtable([folder2 filesep 'emotion.csv']);
        emotion2 = table2cell(emotion2);

        epoch_to_use = [];
        for e = 1:length(epoch_accepted)
            position = epoch_accepted(e);
            if emotion1{position} > 2 && emotion2{position} > 2
                epoch_to_use = [epoch_to_use position];
            end
        end

        %%%% cascade verified epoch %%%%
        x = [];
        for i = 1:length(epoch_to_use)
            [~,~,~,signal] = openeph(epoch_array{epoch_to_use(i),2});
            x = [x ; signal];
        end

        y = [];
        for i = 1:length(epoch_to_use)
            [~,~,~,signal] = openeph(epoch_array{epoch_to_use(i),4});
            y = [y ; signal];
        end
    end
    
end

