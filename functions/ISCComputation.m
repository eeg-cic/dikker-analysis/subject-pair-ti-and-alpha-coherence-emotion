function [ ISC ] = ISCComputation( S1,S2 )

    % Compute ISC score of 2 subjects of 1 Trial
    % Return ans 1 numeric value

    cross_cov1 = corr(S1,S2);
    cross_cov2 = corr(S2,S1);
    
    auto_corr1 = corr(S1,S1);
    auto_corr2 = corr(S2,S2);
    
    Rw = (auto_corr1 + auto_corr2)/2;
    Rb = (cross_cov1 + cross_cov2)/2;
    
    [ek,evalue] = eig(Rw\Rb);
    evalue = sum(evalue);
    [~, evalue_order] = sort(evalue,'descend');
    ek_sorted = ek(:,evalue_order);
    
    Ck1 = (ek_sorted(:,1)'*Rb*ek_sorted(:,1)) / (ek_sorted(:,1)'*Rw*ek_sorted(:,1));
    Ck2 = (ek_sorted(:,2)'*Rb*ek_sorted(:,2)) / (ek_sorted(:,2)'*Rw*ek_sorted(:,2));
    Ck3 = (ek_sorted(:,3)'*Rb*ek_sorted(:,3)) / (ek_sorted(:,3)'*Rw*ek_sorted(:,3));
    
    ISC = Ck1+Ck2+Ck3;

end

